from conan import ConanFile


class KeithleyElectrometersRecipe(ConanFile):
    name = "KeithleyElectrometers"
    executable = "ds_KeithleyElectrometers"
    version = "2.6.2"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"
    author = "X. ELATTAOUI"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "Keithley Electrometer device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("electrometers/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
